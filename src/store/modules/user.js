export const user = {
    state: () => ({
        name: '',
        points: 0,
        correctIds: [],
        incorrectIds: [],
        oldGames: []
    }),
    mutations: {
        setUserName(state, value) {
            state.name = value
        },
        addPoints(state, {id, value}) {
            state.points += value;
            state.correctIds.push(id)
        },
        subtractPoints(state, {id, value}) {
            state.points -= value;
            state.incorrectIds.push(id)
        },
        setOldGames(state, value) {
            state.oldGames = value;
        },
        clearUserCurrentGame(state) {
            state.points = 0;
            state.correctIds = [];
            state.incorrectIds = [];
        }
    },
    getters: {
        getUserData: state => {
            const { correctIds, incorrectIds } = state;
            const correctLength = correctIds.length;
            const incorrectLength = incorrectIds.length;
            const commonLength = correctLength + incorrectLength;
            const points = state.points;

            return {
                commonLength,
                correctLength,
                incorrectLength,
                points
            }

        }

    }
}
