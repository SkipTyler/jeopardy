import {getCategories, getCategory} from "@/actions/jservice";

export const jeopardy = {
    state: () => ({
        questions: null,
        gameIsStart: false
    }),
    mutations: {
        setQuestions(state, value) {
            state.questions = value;
        },
        startGame(state) {
            state.gameIsStart = true;
        },
        stopGame(state) {
            state.gameIsStart = false;
        }
    },
    actions: {
        loadQuestions: async ({commit, rootState}) => {
            try {
                const numberOldGames = rootState.user.oldGames.length;
                const categories = await getCategories(numberOldGames);
                const mappedRequests = categories.map((cat) => getCategory(cat.id));
                const questions = await Promise.all(mappedRequests);
                const reducedQuestions = questions.map(q => ({
                    id: q.id,
                    title: q.title,
                    clues: q.clues.slice(0, 5)
                }));

                commit('setQuestions', reducedQuestions)
            } catch (e) {
                console.log(e);
                console.log(e.response);
            }
        }
    }
}
