import Vue from 'vue'
import Vuex from 'vuex'

import { jeopardy } from "@/store/modules/jeopardy";
import { user } from "@/store/modules/user";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    jeopardy,
    user
  }
})
