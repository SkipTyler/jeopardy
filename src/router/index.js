import Vue from 'vue'
import VueRouter from 'vue-router'
import { routes } from "@/config/constans";

Vue.use(VueRouter)


const router = new VueRouter({
  mode: 'history',
  fallback: false,
  scrollBehavior: () => ({y: 0}),
  linkActiveClass: 'active',
  linkExactActiveClass: 'exactActive',
  routes: [
    {
      path: '/',
      redirect: {name: 'game'},
      component: () => import('@/views/index'),
      children: [
        {
          path: routes.game,
          name: 'game',
          component: () => import('@/views/game')
        },
        {
          path: routes.statistics,
          name: 'statistics',
          component: () => import('@/views/statistics')
        }
      ]
    }
  ]
})

export default router
