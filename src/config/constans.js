const proxyUrl = 'https://cors-anywhere.herokuapp.com/';


export const apiBaseUrl = proxyUrl + 'https://jservice.io/api';

export const apiUrls = {
    categories: 'categories',
    category: 'category'
};

export const routes = {
    game: 'game',
    statistics: 'statistics'
}
