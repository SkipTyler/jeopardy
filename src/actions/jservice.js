import {apiInstance} from "@/actions/index";
import {apiUrls} from "@/config/constans";

export const getCategories = async (offset = 0) => {
    try {
        return (await apiInstance().get(apiUrls.categories, {
            params: { count: 5, offset }
        })).data;
    } catch (e) {
        console.log(e.response);
    }
};

export const getCategory = async (id) => {
    try {
        return (await apiInstance().get(apiUrls.category, {
            params: { id }
        })).data;
    } catch (e) {
        console.log(e.response);
    }
}
