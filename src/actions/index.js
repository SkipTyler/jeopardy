import axios from 'axios';
import {apiBaseUrl} from "@/config/constans";

export const apiInstance = (url = apiBaseUrl) => axios.create({
    baseURL: url
});
