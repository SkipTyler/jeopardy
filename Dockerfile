FROM node:14.4.0
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
RUN npm i http-server -g
CMD [ "http-server", "dist" ]
